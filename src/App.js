import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import RouterComponent from "./utils/Router";

const App = () => {
  return (
    <Router>
      <div>
        <RouterComponent />
      </div>
    </Router>
  );
};

export default App;
