import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home";
import PodcastDetails from "../pages/PodcastDetails";
import EpisodeDetails from "../pages/EpisodeDetails";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/podcast/:podcastId" element={<PodcastDetails />} />
      <Route
        path="/podcast/:podcastId/episode/:episodeId"
        element={<EpisodeDetails />}
      />
    </Routes>
  );
};

export default Router;
