import axios from 'axios';

const podcastApi = axios.create({
  baseURL: process.env.REACT_APP_ITUNES_API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const getPodcasts = async (limit = 100, genre = 1310) => {
  try {
    const response = await podcastApi.get(`/us/rss/toppodcasts/limit=${limit}/genre=${genre}/json`);
    return response.data.feed.entry;
  } catch (error) {
    console.error(error);
    return [];
  }
};