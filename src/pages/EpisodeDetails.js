import React from "react";
import { useParams } from "react-router-dom";

const EpisodeDetails = () => {
  const { podcastId, episodeId } = useParams();

  return (
    <div>
      <h1>
        Detalles del episodio {episodeId} del podcast {podcastId}
      </h1>
      <p>
        Esta es la vista de detalles del episodio {episodeId} del podcast{" "}
        {podcastId}
      </p>
    </div>
  );
};

export default EpisodeDetails;
