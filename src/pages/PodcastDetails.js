import React from "react";
import { useParams } from "react-router-dom";

const PodcastDetails = () => {
  const { podcastId } = useParams();

  return (
    <div>
      <h1>Detalles del podcast {podcastId}</h1>
      <p>Esta es la vista de detalles del podcast {podcastId}</p>
    </div>
  );
};

export default PodcastDetails;
